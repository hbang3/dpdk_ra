#include "ra.h"
#include <time.h>
#include <stdio.h>
 
void get_ra(ra_t * ret, uint8_t * pkt, uint8_t idx) {
	ret->ra_timestamp = ld64(pkt, idx);
	ret->ra_hash = ld32(pkt, idx + 8);
	ret->ing_dna = ld16(pkt, idx + 12);
	ret->eg_dna = ld16(pkt, idx + 14); 
}

void get_ra_edge(ra_edge_t * ret, uint8_t * pkt, uint8_t idx) {
	ret->ra_edge_timestamp = ld64(pkt, idx);
	ret->event_id = ld8(pkt, idx+8);  
	ret->event_cnt = ld40(pkt, idx + 9);
	ret->time_diff = ld16(pkt, idx + 14);
}

void get_ipv6_addr(ipv6_addr_t * ipv6_addr, uint8_t * pkt, uint8_t idx) {
	ipv6_addr->ms_addr = ld64(pkt, idx);
	ipv6_addr->ls_addr = ld64(pkt, idx + 8);
}

void print_date(uint64_t seconds) {
    time_t timestamp = (time_t)seconds;
    struct tm *timeinfo;
    char buffer[80];

    timeinfo = localtime(&timestamp);
    strftime(buffer, sizeof(buffer), "%Y-%m-%d %H:%M:%S", timeinfo);
    printf("decrypted timestamp: %s    ", buffer);
}
  
void print_ra(ra_t * ra) { 
	printf("====================== R A ========================\n");
	printf("ra_timestamp: %ld  ", ra->ra_timestamp);
	printf("ra_hash: 0x%x  ", ra->ra_hash);
	printf("ingress_dna: 0x%x  ", ra->ing_dna);
	printf("egress_dna: 0x%x  \n", ra->eg_dna);
	printf("===================================================\n");
}

void print_decrypted_ra_edge(ra_edge_t * ra_edge) { 
	printf("================== R A _ E D G E ===================\n");
    print_date(ra_edge->ra_edge_timestamp);
	printf("event_id: %x  ", ra_edge->event_id); 
	printf("event_cnt: %ld  ", ra_edge->event_cnt);
	printf("time_diff: %x  \n", ra_edge->time_diff); 
	printf("===================================================\n");
}

void print_encrypted_ra_edge(ra_edge_t * ra_edge) { 
	printf("================== R A _ E D G E ===================\n");
	printf("encrypted_timestamp: %ld  ", ra_edge->ra_edge_timestamp);
	printf("event_id: %x  ", ra_edge->event_id); 
	printf("event_cnt: %ld  ", ra_edge->event_cnt);
	printf("time_diff: %x  \n", ra_edge->time_diff); 
	printf("===================================================\n");
}

void print_ipv6_addr(ipv6_addr_t * ipv6_addr) {
	printf("%lx:%lx", ipv6_addr->ms_addr, ipv6_addr->ls_addr);
}
