#include <inttypes.h>
#include <rte_cycles.h>
#include <rte_eal.h>
#include <rte_ethdev.h>
#include <rte_lcore.h>
#include <rte_mbuf.h>
#include <signal.h>
#include <stdint.h>
#include <stdlib.h>
#include <sys/time.h>
#include "ra.h"

#define RX_RING_SIZE 1024
#define TX_RING_SIZE 1024
#define NUM_MBUFS 8192 * 4
#define MBUF_CACHE_SIZE 512
#define BURST_SIZE 16

struct rte_eth_stats caper_stats;
struct lcore_stat {
    uint64_t interval;
    uint64_t normal;
    uint64_t not_ipv6;
    uint64_t not_srv6;
    uint64_t no_key; 
    uint64_t no_cal;
    uint64_t tstmp_err;
    uint64_t t_pkt; uint64_t f_pkt;
    uint64_t t_pkt_acc;
    uint64_t f_pkt_acc;
    struct timespec start_time;
    struct timespec end_time;
    double throughput_acc;
};

struct lcore_stat lcore_stats[RTE_ETHDEV_QUEUE_STAT_CNTRS];

static const struct rte_eth_conf port_conf_default = { .rxmode =
        {
            .mq_mode = RTE_ETH_MQ_RX_RSS,
        },
    .txmode =
        {
            .mq_mode = RTE_ETH_MQ_TX_NONE,
        },
    .rx_adv_conf = {.rss_conf = {.rss_hf = RTE_ETH_RSS_IP | RTE_ETH_RSS_TCP |
                                           RTE_ETH_RSS_UDP}},
};


static inline int port_init(uint16_t port, struct rte_mempool *mbuf_pool) {
    struct rte_eth_conf port_conf = port_conf_default;
    const uint16_t rx_rings = rte_lcore_count(), tx_rings = 1;
    uint16_t nb_rxd = RX_RING_SIZE;
    uint16_t nb_txd = TX_RING_SIZE;
    int retval;
    uint16_t q;

    struct rte_eth_dev_info dev_info;
    struct rte_eth_txconf txconf;

    if (!rte_eth_dev_is_valid_port(port)) return -1;

    retval = rte_eth_dev_info_get(port, &dev_info);
    if (retval != 0) {
        printf("Error during getting device (port %u) info : %s \n", port,
               strerror(-retval));
        return retval;
    }

    if (dev_info.tx_offload_capa & RTE_ETH_TX_OFFLOAD_MBUF_FAST_FREE) {
        port_conf.txmode.offloads |= RTE_ETH_TX_OFFLOAD_MBUF_FAST_FREE;
    }

    port_conf.rx_adv_conf.rss_conf.rss_hf &= dev_info.flow_type_rss_offloads;
    if (port_conf.rx_adv_conf.rss_conf.rss_hf !=
      port_conf_default.rx_adv_conf.rss_conf.rss_hf) {
        printf("\nport %u modified RSS hash function based on hardware support,\n"
               "requested: %" PRIx64 " configured %" PRIx64 "\n",
               port, port_conf_default.rx_adv_conf.rss_conf.rss_hf,
               port_conf.rx_adv_conf.rss_conf.rss_hf);
    }

    retval = rte_eth_dev_configure(port, rx_rings, tx_rings, &port_conf);
    if (retval != 0) return retval;

    retval = rte_eth_dev_adjust_nb_rx_tx_desc(port, &nb_rxd, &nb_txd);
    if (retval != 0) return retval;

    for (q = 0; q < rx_rings; q++) {
        retval = rte_eth_rx_queue_setup(
            port, q, nb_rxd, rte_eth_dev_socket_id(port), NULL, mbuf_pool);
        if (retval < 0) return retval;
    }

    txconf = dev_info.default_txconf;
    txconf.offloads = port_conf.txmode.offloads;

    for (q = 0; q < tx_rings; q++) {
        retval = rte_eth_tx_queue_setup(port, q, nb_txd,
                                        rte_eth_dev_socket_id(port), &txconf);
        if (retval < 0) return retval;
    }

    retval = rte_eth_dev_start(port);
    if (retval < 0) return retval;

    struct rte_ether_addr addr;
    retval = rte_eth_macaddr_get(port, &addr);
    if (retval != 0) return retval;

    printf("Port %u MAC: %02" PRIx8 " %02" PRIx8 " %02" PRIx8 " %02" PRIx8
         " %02" PRIx8 " %02" PRIx8 "\n",
         port, RTE_ETHER_ADDR_BYTES(&addr));

    retval = rte_eth_promiscuous_enable(port);
    if (retval != 0) return retval;

    return 0;
}

void print_eth_stats(uint8_t port_id, struct rte_eth_stats *stats) {
    uint64_t total_pkt = 0;
    double total_throughput = 0.0;
    rte_eth_stats_get(0, stats);
    printf("\nsuccessfully rx: %" PRIu64 " packets\n", stats->ipackets);
    printf("successfully rx: %" PRIu64 " bytes\n", stats->ibytes);
    printf("erroneous rx: %" PRIu64 "\n", stats->ierrors);
    printf("no mbuf: %" PRIu64 "\n", stats->rx_nombuf);
    printf("missed: %" PRIu64 "\n", stats->imissed);
    int i;
    for (i = 0; i < rte_lcore_count(); i++) {
        printf("rxq: %d, true_pkt: %" PRIu64 "  false_pkt: %" PRIu64 " \n", i,
               lcore_stats[i].t_pkt_acc + lcore_stats[i].t_pkt,
               lcore_stats[i].f_pkt_acc + lcore_stats[i].f_pkt);
        total_pkt += lcore_stats[i].t_pkt;
        total_pkt += lcore_stats[i].f_pkt;
        total_pkt += lcore_stats[i].t_pkt_acc;
        total_pkt += lcore_stats[i].f_pkt_acc;
        total_throughput += (lcore_stats[i].throughput_acc / (lcore_stats[i].interval-1)); 
    }
    printf("%lf, ", (double)total_pkt / (stats->ipackets + stats->rx_nombuf + stats->imissed));
    printf("%lf", total_throughput);
}

void exit_handler(int sig) {
    print_eth_stats(0, &caper_stats);
    exit(1);
}

uint8_t process_packet(uint8_t * pkt) {
    uint8_t idx;
	if (ld16(pkt, 12) != 0x86dd) {
		return RA_NOT_IPV6; 
	}
	if (ld8(pkt, 20)!= 43) {
		return RA_NOT_SRV6; 
	}
    
    idx = 22;
    ipv6_addr_t src_ipv6_addr, dst_ipv6_addr; 
    get_ipv6_addr(&src_ipv6_addr, pkt, idx);
    get_ipv6_addr(&src_ipv6_addr, pkt, idx + 16); 

	idx = 0x3e;
	ra_edge_t edge1, edge2;
	ra_t      ra1, ra2, ra3;

	get_ra_edge(&edge1, pkt, idx);
	get_ra_edge(&edge2, pkt, idx+16);
	get_ra(&ra1, pkt, idx+32);
    
    if (ra1.ra_timestamp == 0xffffffffffffffff) {
        return RA_NO_KEY;
    }
    if ((edge1.ra_edge_timestamp == 0) && (edge1.event_id == 0xff || edge2.event_id == 0xff))  {   
        return RA_NO_CAL_TSTMP_ERR;
    } else if (edge1.event_id == 0xff || edge2.event_id == 0xff) {
        return RA_NO_CAL;
    } else if (edge1.ra_edge_timestamp == 0){
        return RA_TSTMP_ERR;
    }
    
    return RA_NORMAL;
}

void print_stat(struct lcore_stat *stat) {
    printf(
        "| %-10s | %-10s | %-10s | %-10s | %-10s | %-10s |\n" 
        "| %-10" PRIu64 " | %-10" PRIu64 " | %-10" PRIu64 " | %-10" PRIu64 " | %-10" PRIu64 " | %-10" PRIu64 " |\n\n", 
        "normal", "not_ipv6", "not_srv6", "no_key", "no_cal", "tstmp_err",
        stat->normal, stat->not_ipv6, stat->not_srv6, stat->no_key, stat->no_cal, stat->tstmp_err
    );
}

int main_loop() {
    uint64_t tick_cnt;
    uint64_t clock_freq = rte_get_timer_hz();
    double elapsed_time;
    double throughputs;
    struct rte_mbuf *bufs[BURST_SIZE];
    uint16_t nb_rx;
    uint8_t *pkt;
    uint8_t lcore_id = rte_lcore_id();
    uint8_t qid = rte_lcore_index(lcore_id);
    lcore_stats[qid].t_pkt = 0;
    lcore_stats[qid].f_pkt = 0;
    while (1) {
        nb_rx = rte_eth_rx_burst(0, qid, bufs, BURST_SIZE);
        uint16_t i;
        uint8_t result;
        for (i = 0; i < nb_rx; i++) {
            pkt = rte_pktmbuf_mtod(bufs[i], uint8_t *);
            result = process_packet(pkt);
            switch (result) {
                case RA_NORMAL:
                    lcore_stats[qid].normal++;
                    lcore_stats[qid].t_pkt++;
                    break;
                case RA_NO_KEY:
                    lcore_stats[qid].no_key++;
                    lcore_stats[qid].f_pkt++;
                    break;
                case RA_NO_CAL_TSTMP_ERR:
                    lcore_stats[qid].no_cal++;
                    lcore_stats[qid].tstmp_err++;
                    lcore_stats[qid].f_pkt++;
                    break;
                case RA_NO_CAL:
                    lcore_stats[qid].no_cal++;
                    lcore_stats[qid].f_pkt++;
                    break;
                case RA_TSTMP_ERR:
                    lcore_stats[qid].tstmp_err++;
                    lcore_stats[qid].f_pkt++;
                    break;
                default:
                    break;
            }

            if ((lcore_stats[qid].f_pkt + lcore_stats[qid].t_pkt) == 10000000) {  
                clock_gettime(CLOCK_MONOTONIC, &lcore_stats[qid].end_time); 
                if (lcore_stats[qid].interval != 0) {
                    elapsed_time =
                       ((uint64_t)lcore_stats[qid].end_time.tv_sec * 1000LL + (uint64_t)lcore_stats[qid].end_time.tv_nsec / 1000000LL) -
                       ((uint64_t)lcore_stats[qid].start_time.tv_sec * 1000LL + (uint64_t)lcore_stats[qid].start_time.tv_nsec / 1000000LL);
                    lcore_stats[qid].throughput_acc += ((double)10000000 / elapsed_time * 1000);
                } 
                printf("| CORE %d, rxq %d, %lf ms, %ld pkts\n", lcore_id, qid, elapsed_time, lcore_stats[qid].t_pkt + lcore_stats[qid].f_pkt);
                print_stat(&(lcore_stats[qid])); 
                lcore_stats[qid].interval += 1;   
                lcore_stats[qid].t_pkt_acc += lcore_stats[qid].t_pkt;
                lcore_stats[qid].f_pkt_acc += lcore_stats[qid].f_pkt;
                lcore_stats[qid].t_pkt = 0;
                lcore_stats[qid].f_pkt = 0;
                lcore_stats[qid].start_time = lcore_stats[qid].end_time;
            }
            rte_pktmbuf_free(bufs[i]);
        }
    }
}

int main(int argc, char **argv) {
    struct rte_mempool *mbuf_pool;
    uint16_t nb_ports;
    struct rte_eth_conf port_conf;
    signal(SIGINT, exit_handler);

    int ret = rte_eal_init(argc, argv);
    if (ret < 0) rte_exit(EXIT_FAILURE, "EAL INIT ERROR");
    argc -= ret;
    argv += ret;

    nb_ports = rte_eth_dev_count_avail();
    printf("%d numbers of port available", nb_ports);

    mbuf_pool = rte_pktmbuf_pool_create(
      "MBUF_POOL", NUM_MBUFS * nb_ports, MBUF_CACHE_SIZE, 0,
      RTE_MBUF_DEFAULT_BUF_SIZE, rte_socket_id());
    if (mbuf_pool == NULL) rte_exit(EXIT_FAILURE, "MBUF_POOL CREATION FAIL");

    uint16_t port_id = 0;
    ret = port_init(port_id, mbuf_pool);
    if (ret < 0) rte_exit(EXIT_FAILURE, "!!");

    print_eth_stats(0, &caper_stats);
    rte_eth_stats_reset(0);
    rte_eal_mp_remote_launch(main_loop, NULL, CALL_MAIN);
    rte_eal_mp_wait_lcore();
    return 0;
}
