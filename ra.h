#include <stdint.h>

#define RA_NORMAL     0 
#define RA_NOT_IPV6   1 
#define RA_NOT_SRV6   2
#define RA_NO_KEY     3 
#define RA_NO_CAL     4
#define RA_TSTMP_ERR  5
#define RA_NO_CAL_TSTMP_ERR 6

struct ra {
	uint64_t ra_timestamp; 
	uint32_t ra_hash;
	uint16_t  ing_dna;
	uint16_t  eg_dna;
} typedef ra_t;

struct ra_edge {
	uint64_t ra_edge_timestamp; 
	uint8_t event_id;
	uint64_t event_cnt; 
	uint16_t time_diff;
} typedef ra_edge_t;

struct ipv6_addr {
	uint64_t ms_addr; //most_significant address
	uint64_t ls_addr; //less_significant address
} typedef ipv6_addr_t;
  
void get_ra(ra_t * ret, uint8_t * pkt, uint8_t idx);
void get_ra_edge(ra_edge_t * ret, uint8_t * pkt, uint8_t idx);
void get_ipv6_addr(ipv6_addr_t * ipv6_addr, uint8_t * pkt, uint8_t idx);  
void print_ra(ra_t * ra);
void print_decrypted_ra_edge(ra_edge_t * ra_edge);
void print_encrypted_ra_edge(ra_edge_t * ra_edge);
void print_ipv6_addr(ipv6_addr_t * ipv6_addr);   

static inline uint64_t ld64(uint8_t * pkt, uint8_t idx) {
    return ((uint64_t)pkt[idx] << 56) +
	  ((uint64_t)pkt[idx+1] << 48) +
	  ((uint64_t)pkt[idx+2] << 40) +
	  ((uint64_t)pkt[idx+3] << 32) +
	  ((uint64_t)pkt[idx+4] << 24) +
	  ((uint64_t)pkt[idx+5] << 16) +
	  ((uint64_t)pkt[idx+6] << 8) +
	  ((uint64_t)pkt[idx+7]);
} 

static inline uint64_t ld40(uint8_t * pkt, uint8_t idx) {
	return ((uint64_t)pkt[idx] << 32) +
	  ((uint64_t)pkt[idx+1] << 24) +
	  ((uint64_t)pkt[idx+2] << 16) +
	  ((uint64_t)pkt[idx+3] << 8) +
	  ((uint64_t)pkt[idx+4]);
} 

static inline uint32_t ld32(uint8_t * pkt, uint8_t idx) {
    return ((uint32_t)pkt[idx] << 24) +
	  ((uint32_t)pkt[idx+1] << 16) +
	  ((uint32_t)pkt[idx+2] << 8) +
	  ((uint32_t)pkt[idx+3]);
}

static inline uint16_t ld16(uint8_t * pkt, uint8_t idx) {
    return ((uint16_t)pkt[idx] << 8) + ((uint16_t)pkt[idx+1]); 
}

static inline uint8_t ld8(uint8_t * pkt, uint8_t idx) {
	return pkt[idx];
}
